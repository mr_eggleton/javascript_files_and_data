var players = ["Grimwald", "Caitlin", "Cameron","Bernard", "Sean"];
var cards_in_hand = [0, 0, 0, 0, 0]; // How many cards each player has in their hand to start with

var cards = 52; // Cards left in the deck
var current_player_index = 0; //the number / index / position of the current player 
while(cards > 0) { // While we still have cards left in the deck
    if(cards_in_hand[current_player_index] < 5) { // if the current player has less than 5 cards
        cards = cards - 1; // Take a card off the deck
        cards_in_hand[current_player_index] = cards_in_hand[current_player_index] + 1; // add that card to the current players hand
    } else if (Math.round(Math.random())) { // else if tossing a coin gives heads
        cards = cards - 1; // Take a card off the deck
        cards_in_hand[current_player_index] = cards_in_hand[current_player_index] + 1; // add that card to the current players hand
    }
    current_player_index = current_player_index + 1; //Work out who the new current player is by adding 1
    if(current_player_index >= players.length) { // if the next player isn't on the list
       current_player_index = 0; // go back to the first player GOT IT!
    }
}
 
// Print out the results   
for(var i = 0; i < players.length; i++) {
    console.log(players[i] + " has " + cards_in_hand[i] + " cards"); 
}

