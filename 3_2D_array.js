var fs = require('fs');

// http://nodejs.org/api/fs.html#fs_fs_readfilesync_filename_options
var sFileData = fs.readFileSync("data/players.csv").toString();
console.log(sFileData);

// https://www.npmjs.com/package/csv-string
var lines =  sFileData.split("\n") ;

var players = [];
for(var i = 0; i < lines.length; i++) {
    var items = lines[i].split(",");
    var name = items[0];
    var hand = parseInt(items[1], 10);
    players[i] = [name, hand];
}


console.log("Player arrays before dealing out cards\n", players);


var cards = 52;
var playerindex = 0;
while(cards > 0) {
    if(players[playerindex][1] < 5) {
        players[playerindex][1] = players[playerindex][1] + 1;
        cards = cards - 1;
    } else if (Math.round(Math.random())) {
        players[playerindex][1] = players[playerindex][1] + 1;
        cards = cards - 1;
    }
    playerindex = (playerindex + 1) % players.length;
}


console.log("Player objects after dealing out cards\n", players);

for(var i = 0; i < players.length; i++) {
    console.log(players[i][0] + " has " + players[i][1] + " cards"); 
}

var over12 = players.filter(function(player){
    return player[1] > 12;
});


console.log("Players having more than 12 in their hand\n", over12);
var csv = "";
for(var j=0; j< over12.length; j++) {
    csv = csv + over12[j][0] + ","+over12[j][1] + "\n";
}
console.log(csv);
fs.writeFileSync("data/over12-b.csv", csv);