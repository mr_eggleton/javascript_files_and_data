// Get a way of using the file system  (for reading and writing files)
var fs = require('fs');// Read the text in a file into a string  
// The help for this is here http://nodejs.org/api/fs.html#fs_fs_readfilesync_filename_options
var sFileData = fs.readFileSync("data/players.csv").toString();

//Print out the contents of the file
console.log("Content of file ", sFileData);

//Split the string into lines "\n" means end of line / return
var lines =  sFileData.split("\n") ;

var players = [];
var cards_in_hand = [];

for(var i = 0; i < lines.length; i++) { //each line will denote a different player
    var items = lines[i].split(","); //split the line up by comma
    players[i] = items[0];
    cards_in_hand[i] = parseInt(items[1], 10); // make the string with a number in it into a proper number so we can add it up
}

console.log("Player arrays before dealing out cards\n", players);


var cards = 52; // Cards left in the deck
var current_player_index = 0; //the number / index / position of the current player 
while(cards > 0) { // While we still have cards left in the deck
    if(cards_in_hand[current_player_index] < 5) { // if the current player has less than 5 cards
        cards = cards - 1; // Take a card off the deck
        cards_in_hand[current_player_index] = cards_in_hand[current_player_index] + 1; // add that card to the current players hand
    } else if (Math.round(Math.random())) { // else if tossing a coin gives heads
        cards = cards - 1; // Take a card off the deck
        cards_in_hand[current_player_index] = cards_in_hand[current_player_index] + 1; // add that card to the current players hand
    }
    current_player_index = current_player_index + 1; //Work out who the new current player is by adding 1
    if(current_player_index >= players.length) { // if the next player isn't on the list
       current_player_index = 0; // go back to the first player GOT IT!
    }
}

console.log("Player objects after dealing out cards");

// Print out the results   
for(var j = 0; j < players.length; j++) {
    console.log(players[j] + " has " + cards_in_hand[j] + " cards"); 
}


// make a string with all the players with over 12 cards
var output = "";
for(var k = 0; k < players.length; k++) {
    if (cards_in_hand[k] > 12) {
        output += players[k] + "," + cards_in_hand[k] + "\n" // make a line with the name a comma the number and a line break
    }
}
console.log(output); 

// Write the output to a file
fs.writeFileSync("data/over12.csv", output);