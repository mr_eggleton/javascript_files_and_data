var Player = function(name){
    this.name = name;
    this.cards_in_hand = 0;
};

var players = ["George", "Caitlin", "Cameron", "Sean"].map(function(name){ 
    return new Player(name)
});

console.log("Player objects before dealing out cards\n", players);


var cards = 52;
var playerindex = 0;
while(cards > 0) {
    if(players[playerindex].cards_in_hand < 5) {
        players[playerindex].cards_in_hand = players[playerindex].cards_in_hand + 1;
        cards = cards - 1;
    } else if (Math.round(Math.random())) {
        players[playerindex].cards_in_hand = players[playerindex].cards_in_hand + 1;
        cards = cards - 1;
    }
    playerindex = (playerindex + 1) % players.length;
}


console.log("Player objects after dealing out cards\n", players);

for(var i = 0; i < players.length; i++) {
    console.log(players[i].name + " has " + players[i].cards_in_hand + " cards"); 
}

var over12 = players.filter(function(player){
    return player.cards_in_hand > 12;
});


console.log("Players having more than 12 in their cards_in_hand\n", over12);
