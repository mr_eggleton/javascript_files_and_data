var fs = require('fs');

var sFileData = fs.readFileSync("data/players.json").toString();

var players =   JSON.parse(sFileData);

console.log("Player objects before dealing out cards\n", players);


var cards = 52;
var playerindex = 0;
while(cards > 0) {
    if(players[playerindex].hand < 5) {
        players[playerindex].hand = players[playerindex].hand + 1;
        cards = cards - 1;
    } else if (Math.round(Math.random())) {
        players[playerindex].hand = players[playerindex].hand + 1;
        cards = cards - 1;
    }
    playerindex = (playerindex + 1) % players.length;
}


console.log("Player objects after dealing out cards\n", players);

for(var i = 0; i < players.length; i++) {
    console.log(players[i].name + " has " + players[i].hand + " cards"); 
}

var over12 = players.filter(function(player){
    return player.hand > 12;
});


console.log("Players having more than 12 in their hand\n", over12);


fs.writeFileSync("data/over12.json", JSON.stringify(over12));
